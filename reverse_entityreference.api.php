<?php

/**
 * Implements hook_reverse_entityreference_all_alter().
 *
 * This whole function will be cached, so no magic!
 *
 * @see _reverse_entityreference_all()
 */
function hook_reverse_entityreference_all_alter(&$references) {
  // If you forgot to UPPERCASE your widget label, here's your chance!
  $from = 'node:article:field_author';
  $to = 'user:user';
  $references['to'][$to][$from]['label'] = drupal_strtoupper($references['to'][$to][$from]['label']);
  // Only now it won't match that translated string, so you MIGHT have to do that here too.
}

/**
 * Implements hook_reverse_entityreference_widget_info().
 *
 * This whole function will be cached, so no magic!
 *
 * @see reverse_entityreference_reverse_entityreference_widget_info()
 * @see _reverse_entityreference_widgets()
 */
function hook_reverse_entityreference_widget_info() {
  $file = drupal_get_path('module', 'my_module') . '/my_module.rer_widgets.inc';
  return array(
    // Keyed by unique module-prefixed machine name.
    'my_module_my_rer_widget' => array(

      // Mandatory name for public display. Will be translated later.
      'name' => 'My RER widget',

      // Mandatory form callback that creates the widget element/form.
      // @see _reverse_entityreference_select_form() in reverse_entityreference.widgets.inc
      'form_callback' => 'my_module_my_rer_widget_form',

      // Mandatory options callback that returns the widget's allowed values. RER's own might be fine.
      // @see _reverse_entityreference_select_options()
      'options_callback' => 'my_module_my_rer_widget_options', // or
      'options_callback' => 'my_module_my_rer_widget_options_wrapper',

      // Mandatory options callback that returns the widget's current value. RER's own might be fine.
      // @see _reverse_entityreference_select_selected()
      'selected_callback' => 'my_module_my_rer_widget_selected',

      // Optional includable file with the above callback functions. If you need RER's widgets file AND
      // your own, you must create wrapper functions of your own and include your own file.
      'file' => $file,
    ),
  );
}

/**
 * Form callback for 'my_module_my_rer_widget'.
 *
 * The exact format depends on the widget form callback. This one would work for 'checkboxes' etc.
 */
function my_module_my_rer_widget_form($reference, $context) {
  $options = $context['options'];
  $selected = drupal_map_assoc($context['selected']);

  $element = array(
    '#type' => 'my_custom_element',
    '#options' => $options,
    '#selected' => $selected,

    // This one is mandatory!
    '#rer_current_value' => $selected,
  );

  return $element;
}

/**
 * Options callback for 'my_module_my_rer_widget'.
 *
 * The exact format depends on the widget form callback. This one would work for 'checkboxes' etc.
 */
function my_module_my_rer_widget_options($reference, $context) {
  $options = array(54 => 'Sup!');

  return $options;
}

/**
 * Example (options) callback wrapper.
 *
 * All this wrapper does is include the right RER file, which is why your widget can't use RER's
 * callback function directly.
 */
function my_module_my_rer_widget_options_wrapper($reference, $context) {
  module_load_include('inc', 'reverse_entityreference', 'reverse_entityreference.widgets');
  return _reverse_entityreference_select_options($reference, $context);
}

/**
 * Selected options callback for 'my_module_my_rer_widget'.
 *
 * A numeric 1-D array of selected entity ids.
 */
function my_module_my_rer_widget_selected($reference, $context) {
  $selected = array(54);

  return $selected;
}

/**
 * Implements hook_reverse_entityreference_widget_info_alter().
 *
 * @see _reverse_entityreference_widgets()
 */
function hook_reverse_entityreference_widget_info_alter(&$widgets) {
  $widgets['my_module_my_rer_widget']['name'] .= ' = Da Shit!';
}
