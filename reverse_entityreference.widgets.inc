<?php

/**
 * Widget settings form callback for widget 'select'.
 */
function _reverse_entityreference_select_settings_form($settings, &$form, &$form_state) {
  $element['chosen'] = array(
    '#type' => 'checkbox',
    '#title' => t('Invoke Chosen for this &lt;select&gt; element?'),
    '#default_value' => !empty($settings['chosen']),
    '#description' => t('It is your responsibility to load <a target="_blank" href="@url">Chosen</a>!', array(
      '@url' => '//harvesthq.github.io/chosen/',
    )),
  );

  return $element;
}

/**
 * Widget form callback for widgets 'select' and 'radios'.
 */
function _reverse_entityreference_select_form($reference, $context) {
  $options = $context['options'];
  $selected = drupal_map_assoc($context['selected']);

  $chosen = $reference['widget'] == 'select' && !empty($reference['widget_settings']['chosen']);

  $attributes = array();
  if ($chosen) {
    $attributes['class'][] = 'rer-select-chosen';
    static $invoked_chosen = FALSE;
    if (!$invoked_chosen) {
      drupal_add_js("jQuery(function($) { try { $('select.rer-select-chosen').chosen(); } catch (ex) {} });", array('scope' => 'footer', 'type' => 'inline'));
      $invoked_chosen = TRUE;
    }
  }

  return array(
    '#type' => $reference['widget'],
    '#title' => t($reference['label'], $reference['label_params']),
    '#options' => $options,
    '#default_value' => $selected,
    '#multiple' => TRUE,
    '#size' => max(3, count($options)),
    '#description' => _reverse_entityreference_widget_description($reference),
    '#attributes' => $attributes,

    // The validator needs this to compare the new selection to the old selection.
    '#rer_current_value' => $selected,
  );
}

/**
 * Options callback for widgets 'select' and 'radios'.
 */
function _reverse_entityreference_select_options($reference, $context) {
  $target_bundles = array($reference['bundle'] => $reference['bundle']);
  if ($reference['entity_type'] == 'user') {
    $target_bundles = array();
  }

  $fake_er_field = array(
    'settings' => array(
      'handler' => 'base',
      'target_type' => $reference['entity_type'],
      'handler_settings' => array(
        'target_bundles' => $target_bundles,
        // 'sort' => $reference['sort'], // doesn't work with different base tables
      ),
    ),
  );
  $options = entityreference_get_selection_handler($fake_er_field)->getReferencableEntities();
  $options = reset($options) ?: array();
  unset($options['0']);
  _options_prepare_options($options, array('html' => FALSE, 'strip_tags' => FALSE, 'strip_tags_and_unescape' => FALSE, 'filter_xss' => FALSE));

  return $options;
}

/**
 * Selected options callback for widgets 'select' and 'radios'.
 */
function _reverse_entityreference_select_selected($reference, $context) {
  $eid = $context['eid'];
  $langcode = $context['langcode'];

  $table = 'field_data_' . $reference['field_name'];
  $column = $reference['field_name'] . '_target_id';
  $selected = db_query('
    SELECT entity_id
    FROM {' . $table . '}
    WHERE
      language IN (?, ?) AND
      ' . $column . ' = ? AND
      entity_type = ? AND
      bundle = ?
  ', array(LANGUAGE_NONE, $langcode ?: LANGUAGE_NONE, $eid, $reference['entity_type'], $reference['bundle']))->fetchCol();

  return $selected;
}
